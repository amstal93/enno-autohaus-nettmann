<?php

function showAccessTries()
{
    $db_instance = DatabaseConnectionNettmann::getInstance();
    $accesstries = $db_instance->getAccessTries();

    $data = "<div><table style=\"border-collapse: collapse\" border=\"1\" cellpadding=\"5\">";
    $data .= "<tr bgcolor=\"lightgrey\" align=\"left\">";
    $data .= "<th>ID</th>";
    $data .= "<th>Uhrzeit</th>";
    $data .= "<th>Chip</th>";
    $data .= "<th>Chip Besitzer</th>";
    $data .= "<th>Tür</th>";
    $data .= "<th>Ergebnis</th>";
    $data .= "</tr>";
    foreach ($accesstries as $row) {
        $data .= "<tr>";
        $data .= "<td align=right>". $row->accesstriesID . "</td>";
        $data .= "<td>" . $row->timestamp . "</td>";
        $data .= "<td align=right>" . $row->chips_chipID . "</td>";
        $data .= "<td>Unkown</td>";
        $data .= "<td>Unkown</td>";
        if ($row->accessresult == "Zutritt abgelehnt") {
            $color = "red";
        } else {
            $color = "green";
        }
        $data .= "<td><font color='" . $color . "'>" . $row->accessresult . "</font></td>";
        $data .= "</tr>";
    }
    $data .= "</table></div>";
    $data .= "<div><p>Es ist zu beachten, dass das RFID System sich noch in der Test-Phase befindet!</p></div>";
    return $data;
}
