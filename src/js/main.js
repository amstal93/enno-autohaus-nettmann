var currentTab = 0; // Current tab is set to be the first tab (0)

function showTab(n) {
    // This function will display the specified tab of the form...
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    //... and fix the Previous/Next buttons:
    if (n === 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n === (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    //... and run a function that will display the correct step indicator:
    fixStepIndicator(n)
}

function nextPrev(n) {
    // This function will figure out which tab to display
    // Exit the function if any field in the current tab is invalid:
    switch (currentTab) {
        case 0:
            // Check Radio Buttons
            if (!validateRadioButtonChecked("car_class")) {
                alert("Bitte wähle eine Autoklasse aus!");
                return;
            }
            break;
        case 1:
            // Check Select
            break;
        case 2:
            // Check Checkboxes
            if (!validateSupplementaryEquipmentCheckboxes()) {
                alert("Bitte hake mindestens eine Checkbox an!");
                return;
            }
            break;
        case 3:
            // Check Radion Buttons
            if (!validateRadioButtonChecked("carpark")) {
                alert("Bitte wähle eine Abholstation aus!");
                return;
            }
            break;
    }
    var x = document.getElementsByClassName("tab");
    // Hide the current tab:
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    // if you have reached the end of the form...
    if (currentTab >= x.length) {
        // ... the form gets submitted:
        document.getElementById("regForm").submit();
        return false;
    }
    // Otherwise, display the correct tab:
    showTab(currentTab);
}

function fixStepIndicator(n) {
    // This function removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    //... and adds the "active" class on the current step:
    x[n].className += " active";
}

function resetForm() {
    var x = document.getElementsByClassName("tab");
    x[1].style.display = "none";
    x[2].style.display = "none";
    x[3].style.display = "none";
    currentTab = 0;
    showTab(0);
    fixStepIndicator(0);
}

function validateSupplementaryEquipmentCheckboxes() {
    var c = document.getElementById('checkbox-supplementaryequipment');
    for (var i = 0; i < c.childNodes.length; i++) {
        if (c.childNodes[i].childNodes[0].type === 'checkbox') {
            if (c.childNodes[i].childNodes[0].checked) {
                return true;
            }
        }
    }
    return false;
}

function validateRadioButtonChecked(name) {
    // All <input> tags...
    var chx = document.getElementsByTagName('input');
    for (var i = 0; i < chx.length; i++) {
        if (chx[i].type === 'radio' && chx[i].checked && chx[i].name === name) {
            return true;
        }
    }
    // End of the loop, return false
    return false;
}

window.addEventListener('load', function () {
    showTab(currentTab); // Display the current tab
});
