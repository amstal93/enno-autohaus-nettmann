-- Database
DROP DATABASE IF EXISTS `autohaus-nettmann`;
CREATE DATABASE IF NOT EXISTS `autohaus-nettmann` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `autohaus-nettmann`;

-- Tables
CREATE TABLE IF NOT EXISTS `users`
(
    `userID`   int(11) NOT NULL AUTO_INCREMENT,
    `lastname` varchar(45) DEFAULT NULL,
    `surname`  varchar(45) DEFAULT NULL,
    `group`    int(11)     DEFAULT NULL,
    `password` CHAR(128)   DEFAULT NULL,
    PRIMARY KEY (`userID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 11;

CREATE TABLE IF NOT EXISTS `chips`
(
    `chipID`           int(11) NOT NULL AUTO_INCREMENT,
    `chipserialnumber` varchar(14) DEFAULT NULL,
    `users_userID`     int(11)     DEFAULT NULL,
    PRIMARY KEY (`chipID`),
    KEY `fk_tblChips_tblBenutzer_BenutzerID` (`users_userID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `access_tries`
(
    `accesstriesID` int(11)   NOT NULL AUTO_INCREMENT,
    `timestamp`     timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `chips_chipID`  int(11)        DEFAULT NULL,
    `accessresult`  varchar(45)    DEFAULT NULL,
    `dbuser`        varchar(45)    DEFAULT NULL,
    PRIMARY KEY (`accesstriesID`),
    KEY `fk_tblZutrittsversuche_tblChips_ChipsID` (`chips_chipID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 2359;

CREATE TABLE IF NOT EXISTS `suplementary_equipment`
(
    `ID`          int(11)     NOT NULL AUTO_INCREMENT,
    `name`        varchar(45) NOT NULL,
    `description` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `carparks`
(
    `ID`        int(11)     NOT NULL AUTO_INCREMENT,
    `name`      varchar(45) NOT NULL,
    `latitude`  DECIMAL(10, 8) DEFAULT NULL,
    `longitude` DECIMAL(11, 8) DEFAULT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `car_classes`
(
    `ID`   int(11)     NOT NULL AUTO_INCREMENT,
    `name` varchar(45) NOT NULL,
    PRIMARY KEY (`ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `cars`
(
    `ID`           int(11)     NOT NULL AUTO_INCREMENT,
    `manufacturer` varchar(45) NOT NULL,
    `model`        varchar(45) NOT NULL,
    `car_class_ID` int(11) NOT NULL,
    `rented`       BOOLEAN NOT NULL,
    `location`     int(11) NOT NULL,
    `pricePerDay`  DECIMAL(13, 4) NOT NULL,
    PRIMARY KEY (`ID`),
    KEY `fk_car_class_cars_car_class_ID` (`car_class_ID`),
    KEY `fk_carparks_cars_location` (`location`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `cars_supplementary_equipment`
(
    `ID`                         int(11) NOT NULL AUTO_INCREMENT,
    `car_ID`                     int(11) NOT NULL,
    `supplementary_equipment_ID` int(11) NOT NULL,
    PRIMARY KEY (`ID`),
    KEY `fk_cars_supplementary_equipment_cars_car_ID` (`car_ID`),
    KEY `fk_cars_supp_equipment_supp_equipment_supp_equipment_ID` (`supplementary_equipment_ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

CREATE TABLE IF NOT EXISTS `rental_booking`
(
    `ID`              int(11) NOT NULL AUTO_INCREMENT,
    `days`            int(11) NOT NULL,
    `total_price`     DECIMAL(13, 4) NOT NULL,
    `timestamp_start` timestamp NOT NULL,
    `timestamp_end`   timestamp NOT NULL,
    `user_ID`         int(11) NOT NULL,
    `car_ID`          int(11) NOT NULL,
    PRIMARY KEY (`ID`),
    KEY `fk_rental_booking_user_user_ID` (`user_ID`),
    KEY `fk_rental_booking_cars_car_ID` (`car_ID`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 16;

-- Constraints
ALTER TABLE `chips`
    ADD CONSTRAINT `fk_tblChips_tblBenutzer_BenutzerID`
        FOREIGN KEY (`users_userID`)
            REFERENCES `users` (`userID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `access_tries`
    ADD CONSTRAINT `fk_tblZutrittsversuche_tblChips_ChipsID`
        FOREIGN KEY (`chips_chipID`)
            REFERENCES `chips` (`chipID`)
            ON DELETE SET NULL ON UPDATE NO ACTION;

ALTER TABLE `cars`
    ADD CONSTRAINT `fk_car_class_cars_car_class_ID`
        FOREIGN KEY (`car_class_ID`)
            REFERENCES `car_classes` (`ID`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;

ALTER TABLE `cars`
    ADD CONSTRAINT `fk_carparks_cars_location`
        FOREIGN KEY (`location`)
            REFERENCES `carparks` (`ID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `cars_supplementary_equipment`
    ADD CONSTRAINT `fk_cars_supplementary_equipment_cars_car_ID`
        FOREIGN KEY (`car_ID`)
            REFERENCES `cars` (`ID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `cars_supplementary_equipment`
    ADD CONSTRAINT `fk_cars_supp_equipment_supp_equipment_supp_equipment_ID`
        FOREIGN KEY (`supplementary_equipment_ID`)
            REFERENCES `suplementary_equipment` (`ID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `rental_booking`
    ADD CONSTRAINT `fk_rental_booking_user_user_ID`
        FOREIGN KEY (`user_ID`)
            REFERENCES `users` (`userID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `rental_booking`
    ADD CONSTRAINT `fk_rental_booking_cars_car_ID`
        FOREIGN KEY (`car_ID`)
            REFERENCES `cars` (`ID`)
            ON DELETE NO ACTION ON UPDATE NO ACTION;