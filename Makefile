prod-all: prod-build prod-run
devel-all: devel-build devel-run

prod-build:
	docker build -f docker/standalone.dockerfile -t enno-autohaus-nettmann:latest .

prod-run:
	docker-compose -f docker/docker-compose-standalone.yml up -d

devel-build:
	docker build -f docker/devel.dockerfile -t enno-autohaus-nettmann:devel .

devel-run:
	docker-compose -f docker/docker-compose-devel.yml up -d
