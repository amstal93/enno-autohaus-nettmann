# Autohaus Nettmann

Naming and content on the website is german because this is an educational project for Berufsschule.

Development was done in PHP-Storm.

## Lessons learnt

This application should have the following features to consider it done for our teachers:

- HTML-Forms
- Sending the form the webserver and redirecting to another page which shows this data
- PHP-Sessions
- PHP-Functions

## Features

In our example with the "Autohaus Nettmann" this translates to the following must-have features:

- [x] We want a login form which then enables us to display a calendar and the tried access to the company. (Internal Login)
- [x] We want a login form which then enables us to book a rental car. (External Login)
- [x] The form to book a rental car.
- [x] The list with the list of tried entries into the building.
- [x] The database connection for retrieval of information.
- [x] A Single-Page-Application (SPA) style exchange of information on the site. 

Possible reasonable optional features which I can think of (but may not implement):

- [ ] About page for information on usage and disclaimer.
- [ ] Define an internal form for creation of more cars.
- [x] Manipulate the webserver via `mod_rewrite` to have nice URLs.
- [ ] Enter the user/password combination into the database (with roles/groups).
- [x] Save the password only as a hash in the database.
- [ ] User Self-Registration.
- [ ] Order History for a customer.
- [ ] Cancel an order via the web-interface.
- [ ] Overview page of all booked cars.

TODO:

- Filter the Different cars etc. with Javascript via the `data-*` tags and `value` tags so only valid combinations are
  shown. 

## Things used in this repository for reaseach

- Database: https://www.w3schools.com/php/php_mysql_intro.asp
- Singelton for the database: https://designpatternsphp.readthedocs.io/de/latest/Creational/Singleton/README.html
- Calendar was taken from and improved: https://gitlab.com/B3-Azubis-18/if11b/kalendar-enno
- Inspiration for `.htaccess`: https://www.peterkropff.de/allgemeines/htaccess/htaccess.htm
- More for `.htaccess`: https://stackoverflow.com/q/27637712/4730773 
- Some parts of the code were inspired by: https://github.com/eliroca/autohaus-nettmann/
- Error handling during include: https://andy-carter.com/blog/difference-between-include-and-require-statements-in-php
- CSS Flexbox: https://www.w3schools.com/css/css3_flexbox.asp
- Constants: https://www.php.net/manual/de/function.define.php
- Lat/Lon in MariaDB/MySQL: https://stackoverflow.com/a/12504340/4730773
- Order Form: https://www.w3schools.com/howto/howto_js_form_steps.asp
- Data Attributes: https://www.w3schools.com/tags/att_data-.asp
- JS-Data-Attributes: https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes

## Deployment

This project can be deployed via the `Makefile` in the project root. The commands if `make` is not available can be
copied under an environment with docker. Currently there are no instructions available for non-docker-environments.

Basic-Example: `make`
Deploy & Build Devel: `make Devel-All`

## Password-Generation

````php
echo generatePassword("nichtnettmann");
````
